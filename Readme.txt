Manuel d'utilisation du FrameWork proposé.

Prérequis:

Pour pouvoir utiliser ce framework il vous faut le préprocesseur Compass, pour pouvoir
manipuler des fichiers SCSS.

Il existe 3 types de fichier : 
	- les fichiers bibliothèque (exemple :  "_typo.scss"),
	- les fichiers variables dans le dossier config (exemple :  "_var_typo.scss"),
	- les fichiers exemples (exemple :  "typo.html").
	
	
Utilisation
	
	Installer le dossier "frameWork" dans votre dossier ou vous gerez les feuilles 
style, créer votre nouvelle feuille de style (en .scss) et importer l'ensemble des bibli-
othèques que vous voulez utiliser, ou directement un import de toutes les bibliothèques 
par l'intermédiare de " @import "framework/scss/framework.scss".

Maintenant vous pouvez éditer votre feuille de style attaché a l'abre html que vous avez,
pour ce faire :

.votre_classe {
	@extend class_du_framework
}

Vous pouvez également completer la classe pour qu'elle corresponde a vos besoin. Il faut 
également paramêtrer le framework en éditant les fichier "framework/scss/config/" et après 
compilation du fichier "framework/scss/framework.scss" en utilisant la commande :
"sass framework/scss/framework.scss:framework/framework.css" vous pouvez regarder grace 
au exemple html propose le rendu de votre parametrage.


Voici une liste des fonctionnalités disponible :

Reset :
Un reset de style de Richard Clark

TYPOGRAPHIE : 
Des classe pour les titres de H1 a H6 sont proposés, la taille et les couleurs sont para-
metrable dans le fichier config ( .h1, .h2, .h3, .h4, .h5, .h6).
Une gestion de la police pour le body et les paragraphe (parametrable) 
Une gestion (basique) des balises Inline 
Un parametrage du surlignage avec la balise <mark>
Une mise en page non parametrable pour les citations et les lignes de code
Des classe pour le positionnement du texte :
-.align_left 
-.align_right
-.align_center 
-.align_justify 
Des classe pour la modification du texte:
-.caps_lock_on 
-.caps_lock_off 

ALERTE :
4 classe de messages pour prevenir l'utilisateur avec des couleur parametrable:
.alert_succes 
.alert_informe 
.alert_warning
.alert_danger


BOUTON :
la classe .btn pour créer un bouton normal (parametrable)
.btn-lg et .btn-small pour jouer sur la taille du bouton

.btn-red , btn-black, btn-pink des couleurs pres enregistrer

.btn-extra , btn-flat pour modifier le rendu.


GRID :

Permet de créer une grille fluide (en pourcentage) d'un nombre de colonne et marge para-
metrable. Ensuite pour positionner les éllements il suffit d'appeller 

.grid-container (pour le conteneur de la grille)
.span-x pour un élement de taille x colonne
.offset-x pour décaller l'élément de x colonne

NAV:

Permet de créer un menu responsive facilement

JUMBOTRON :
Fix la présentation d'un jumbotron

MEDIA :
3 classe .mobile .tablette et .pc qui rende visible les éléments uniquement pour un type
de media souhaiter.